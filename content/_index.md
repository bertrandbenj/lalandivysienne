+++
title = "La landivysienne"
outputs = ["Reveal"]
+++


<h4>Anita et Jean Pierre vous accueillent à</h4>

<div class="r-hstack">
  <img src="images/logo.jpg" class="r-stretch" /> 
     <div class="horaires">
        <ul>
            <li>Lundi : 8h - 14h</li>
            <li>Mardi : 8h - 14h</li>
            <li>Mercredi : 8h - 14h</li>
            <li>Jeudi : 8-14h & 17-19h</li>
            <li>Vendredi : 8-14h & 17-23h30</li>
            <li>Samedi : 8-14h & 17-23h30</li>
            <li>Dimanche : 8h30 - 12h</li>
        </ul>
    </div>
</div>

<style>
    .horaires{
        font-size:16px;
        width:300px;
    }
    .horaires > ul{
        list-style: none;
    }
</style>
